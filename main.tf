locals {
  aws_account_id = "679360049118"
  cluster_name   = "cloudservices-ct"
  aws_region     = "eu-west-1"
}

provider "aws" {
  region  = local.aws_region
}

provider "kubernetes" {
  host                   = module.eks.cluster_endpoint
  cluster_ca_certificate = base64decode(module.eks.cluster_certificate_authority_data)
  token                  = data.aws_eks_cluster_auth.cluster.token
}

provider "kubectl" {
  host                   = module.eks.cluster_endpoint
  cluster_ca_certificate = base64decode(module.eks.cluster_certificate_authority_data)
  token                  = data.aws_eks_cluster_auth.cluster.token
}

provider "gitlab" {
  token = var.GITLAB_TOKEN

}

module "aws-policies" {
  source       = "./modules/aws-policies"
  cluster_name = local.cluster_name
  tags = {
    "kubernetes.io/cluster" = local.cluster_name
  }
}

module "flux-irsa" {
  source          = "./modules/eks-irsa"
  namespace       = "flux-system"
  serviceAccount  = "kustomize-controller"
  cluster_name    = local.cluster_name
  aws_account_id  = local.aws_account_id
  oidc_issuer_url = replace(module.eks.cluster_oidc_issuer_url, "https://", "")
  policy_arn      = module.aws-policies.fluxcd_kms_policy
}

module "cert-manager-irsa" {
  source          = "./modules/eks-irsa"
  namespace       = "cert-manager"
  serviceAccount  = "cert-manager"
  cluster_name    = local.cluster_name
  aws_account_id  = local.aws_account_id
  oidc_issuer_url = replace(module.eks.cluster_oidc_issuer_url, "https://", "")
  policy_arn      = module.aws-policies.cert_manager_policy
}

module "external-dns-irsa" {
  source          = "./modules/eks-irsa"
  namespace       = "external-dns"
  serviceAccount  = "external-dns"
  cluster_name    = local.cluster_name
  aws_account_id  = local.aws_account_id
  oidc_issuer_url = replace(module.eks.cluster_oidc_issuer_url, "https://", "")
  policy_arn      = module.aws-policies.external_dns_policy
}

module "flux" {
  source          = "./modules/flux"
  gitlab_owner    = var.GITLAB_OWNER
  target_path     = "clusters/eks"
  cluster_name    = local.cluster_name
  repository_name = "sistemi-cloud/infrastructure-gitops"
  use_irsa        = true
  role_arn        = module.flux-irsa.irsa_role
  branch          = "master"
}

resource "kubernetes_config_map_v1" "aws_irsa_config_map" {
  metadata {
    name      = "aws-irsa"
    namespace = "flux-system"
  }

  data = {
    flux_role         = module.flux-irsa.irsa_role
    cert_manager_role = module.cert-manager-irsa.irsa_role
    external_dns_role = module.external-dns-irsa.irsa_role
  }
}
