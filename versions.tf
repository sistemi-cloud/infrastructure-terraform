terraform {
  required_version = ">= 0.12.9"

  backend "http" {
      address = "https://gitlab.com/api/v4/projects/39367428/terraform/state/eks"
      lock_address = "https://gitlab.com/api/v4/projects/39367428/terraform/state/eks/lock"
      unlock_address = "https://gitlab.com/api/v4/projects/39367428/terraform/state/eks/lock"
      lock_method = "POST"
      unlock_method = "DELETE"
      retry_wait_min = "5"
    }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.15.1"
    }

    kubernetes = {
      source = "hashicorp/kubernetes"
      # version = "2.11.0"
      version = "2.10"
    }

    kubectl = {
      source  = "gavinbunney/kubectl"
      version = "1.14.0"
    }

    gitlab = {
      source  = "gitlabhq/gitlab"
      version = ">= 3.18.0"
    }

    flux = {
      source  = "fluxcd/flux"
      version = "0.15.1"
    }

    tls = {
      source  = "hashicorp/tls"
      version = "3.0"
    }
  }
}
