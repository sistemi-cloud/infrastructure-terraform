module "eks" {
  source          = "terraform-aws-modules/eks/aws"
  version         = "18.29.1"

  cluster_name    = local.cluster_name
  cluster_version = "1.22"

  subnet_ids      = module.vpc.public_subnets
  # subnet_ids      = module.vpc.public_subnets
  vpc_id          = module.vpc.vpc_id

  eks_managed_node_groups = {
    worker = {
      ami_type     = "AL2_x86_64"
      disk_size    = 20
      min_size     = 1
      max_size     = 1
      desired_size = 1

      instance_types = ["c5.xlarge"]
      # instance_types = ["c5.xlarge"]
      capacity_type  = "ON_DEMAND"
      platform       = "linux"

      labels = {
        group-type = "worker"
      }

      security_group_rules = {
        egress_ssh_22 = {
          protocol = "TCP"
          from_port = 22
          to_port = 22
          type = "egress"
          description = "Egress SSH to internet (fluxcd)"
          cidr_blocks = ["0.0.0.0/0"]
        }
        egress_dns_tcp_53 = {
          protocol = "TCP"
          from_port = 53
          to_port = 53
          type = "egress"
          description = "Egress dns to internet (cert-manager)"
          cidr_blocks = ["0.0.0.0/0"]
        }
        egress_dns_udp_53 = {
          protocol = "UDP"
          from_port = 53
          to_port = 53
          type = "egress"
          description = "Egress dns to internet (cert-manager)"
          cidr_blocks = ["0.0.0.0/0"]
        }
        ingress_cluster_15017 = {
          description                   = "Cluster API to node groups (istio)"
          protocol                      = "tcp"
          from_port                     = 15017
          to_port                       = 15017
          type                          = "ingress"
          source_cluster_security_group = true
        }
      }
    }
    database = {
      ami_type     = "AL2_x86_64"
      disk_size    = 20
      min_size     = 1
      max_size     = 1
      desired_size = 1

      instance_types = ["r5.xlarge"]
      capacity_type  = "ON_DEMAND"
      platform       = "linux"

      labels = {
        group-type = "database"
      }

      taints = {
        group-type = {
          key    = "group-type"
          value  = "database"
          effect = "NO_SCHEDULE"
        }
      }
    }
  }

  node_security_group_additional_rules = {
    egress_self_mongodb_27017 = {
      protocol = "TCP"
      from_port = 27017
      to_port = 27017
      type = "egress"
      description = "Node to node MongoDB"
      self = true
    }
    ingress_self_mongodb_27017 = {
      protocol = "TCP"
      from_port = 27017
      to_port = 27017
      type = "ingress"
      description = "Node to node MongoDB"
      self = true
    }
  }

  tags = {
    "kubernetes.io/cluster" = local.cluster_name
  }
}

data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}

data "aws_iam_policy_document" "nlb_policy_document" {
  statement {
    effect  = "Allow"
    sid = "kopsK8sNLBMasterPermsRestrictive"
    actions = [
      "ec2:DescribeVpcs",
      "elasticloadbalancing:AddTags",
      "elasticloadbalancing:CreateListener",
      "elasticloadbalancing:CreateTargetGroup",
      "elasticloadbalancing:DeleteListener",
      "elasticloadbalancing:DeleteTargetGroup",
      "elasticloadbalancing:DescribeListeners",
      "elasticloadbalancing:DescribeLoadBalancerPolicies",
      "elasticloadbalancing:DescribeTargetGroups",
      "elasticloadbalancing:DescribeTargetHealth",
      "elasticloadbalancing:ModifyListener",
      "elasticloadbalancing:ModifyTargetGroup",
      "elasticloadbalancing:RegisterTargets",
      "elasticloadbalancing:SetLoadBalancerPoliciesOfListener"
    ]
    resources = [ "*" ]
  }

  statement {
    effect  = "Allow"
    actions = [
      "ec2:DescribeVpcs",
      "ec2:DescribeRegions"
    ]
    resources = [ "*" ]
  }
}

resource "aws_iam_policy" "nlb_policy" {
  name = "${local.cluster_name}-nlb-policy"
  policy = data.aws_iam_policy_document.nlb_policy_document.json

  tags = {
    "kubernetes.io/cluster" = local.cluster_name
  }
}

resource "aws_iam_role_policy_attachment" "cluster_nlb_policy_attachment" {
  policy_arn = aws_iam_policy.nlb_policy.arn
  role       = module.eks.cluster_iam_role_name
}
