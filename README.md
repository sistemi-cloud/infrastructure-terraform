# Infrastructure Terraform
Mange and deploy a flux based kubernetes cluster on aws cloud using terraform.

### Dependencies
- aws-cli https://aws.amazon.com/it/cli/
- terraform https://www.terraform.io/

### How to deploy
Create a terraform.tfvars file with valid gitlab credentials:
```terraform
GITLAB_OWNER = "exampleOwner"
GITLAB_TOKEN = "glpat-example-personal-access-token"
```
These variables need to be exported in bash before init.
```bash
export GITLAB_OWNER="exampleOwner"
export GITLAB_TOKEN="glpat-example-personal-access-token"
```
You have to configure aws-cli before launching terraform (https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html).
To deploy the infrastructure:
```bash
    terraform init -backend-config=username=${GITLAB_OWNER} -backend-config=password=${GITLAB_TOKEN}
    terraform plan
    terraform apply
```
If you want to fully delete the deployed infrastructure:
```bash
    terraform destroy
```
This project is configured to use gitlab for remote state synchronization.