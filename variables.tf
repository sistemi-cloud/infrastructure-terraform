variable "GITLAB_OWNER" {
  description = "gitlab owner"
  type        = string
}

variable "GITLAB_TOKEN" {
  description = "gitlab token"
  type        = string
  sensitive   = true
}
