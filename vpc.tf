data "aws_availability_zones" "available" {}

/*
resource "aws_vpc" "vpc" {
  enable_dns_hostnames = true
  enable_dns_support   = true

  cidr_block = "172.32.0.0/16"

  // "10.0.0.0/16"
  instance_tenancy = "default"
  # instance_tenancy = var.vpc.instance_tenancy
  // "default"

  tags = {
    "kubernetes.io/cluster/${local.cluster_name}" = "shared"
    "kubernetes.io/cluster"                       = local.cluster_name
  }
  #  tags = {
  #    name = var.vpc.tag_name
  #    //"psm-production-1"
  #  }
}
*/

module "vpc" {
  # source  = "registry.terraform.io/terraform-aws-modules/vpc/aws"
  source = "terraform-aws-modules/vpc/aws"
  version = "3.16.0"

  name           = "${local.cluster_name}-vpc"
  cidr           = "172.32.0.0/16"
  azs            = data.aws_availability_zones.available.names
  public_subnets = ["172.32.16.0/20", "172.32.32.0/20", "172.32.0.0/20"]

  tags = {
    "kubernetes.io/cluster/${local.cluster_name}" = "shared"
    "kubernetes.io/cluster"                       = local.cluster_name
  }

  public_subnet_tags = {
    "kubernetes.io/cluster/${local.cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb"             = "1"
    "kubernetes.io/cluster"                       = local.cluster_name
  }

  enable_dns_hostnames = true
  enable_dns_support   = true
}
