terraform {
  required_version = ">= 0.13"

  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = ">= 3.72"
      #version = ">= 4.15.1"
    }

    kubernetes = {
      source = "hashicorp/kubernetes"
      version = "2.10"
      #version = "2.11.0"
    }

    kubectl = {
      source = "gavinbunney/kubectl"
      version = "1.14.0"
    }

    gitlab = {
      source  = "gitlabhq/gitlab"
      version = ">= 3.11.1"
    }

    flux = {
      source = "fluxcd/flux"
      version = "0.15.1"
    }

    tls = {
      source  = "hashicorp/tls"
      version = "3.0"
      # version = "3.1.0"
    }
  }
}
