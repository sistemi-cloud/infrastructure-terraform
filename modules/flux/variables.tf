variable "gitlab_owner" {
  description = "gitlab owner"
  type        = string
}

variable "repository_name" {
  description = "gitlab repository name"
  type        = string
  default     = "infrastructure-terraform"
}

variable "branch" {
  description = "branch name"
  type        = string
  default     = "main"
}

variable "target_path" {
  description = "flux sync target path"
  type        = string
  default     = "clusters/local"
}

variable "cluster_name" {
  type        = string
  default     = "cluster-local"
}

variable "use_irsa" {
  type = bool
  default = false
}

variable "role_arn" {
  type = string
  default = ""
}