data "aws_iam_policy_document" "policy_document" {
  statement {
    actions = ["sts:AssumeRoleWithWebIdentity"]
    effect  = "Allow"

    condition {
      test     = "StringEquals"
      variable = "${var.oidc_issuer_url}:sub"
      values   = ["system:serviceaccount:${var.namespace}:${var.serviceAccount}"]
    }

    condition {
      test     = "StringEquals"
      variable = "${var.oidc_issuer_url}:aud"
      values   = ["sts.amazonaws.com"]
    }

    principals {
      identifiers = ["arn:aws:iam::${var.aws_account_id}:oidc-provider/${var.oidc_issuer_url}"]
      type        = "Federated"
    }
  }
}

resource "aws_iam_role" "role" {
  assume_role_policy = data.aws_iam_policy_document.policy_document.json
  name               = "${var.cluster_name}-${var.serviceAccount}-${var.namespace}-role"
}

resource "aws_iam_role_policy_attachment" "attach" {
  policy_arn = var.policy_arn
  role       = aws_iam_role.role.name
}