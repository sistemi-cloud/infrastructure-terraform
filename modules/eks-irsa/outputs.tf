output "namespace" {
  description = "The name of the related namespace"
  value       = var.namespace
}

output "serviceAccount" {
  description = "The name of the related serviceaccount"
  value       = var.serviceAccount
}

output "irsa_role" {
  description = "The name of finegrained IAM role created"
  value       = aws_iam_role.role.arn
}