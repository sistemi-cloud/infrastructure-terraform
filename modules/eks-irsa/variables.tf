variable "namespace" {
  description = "Name of Kubernetes namespace"
}

variable "serviceAccount" {
  description = "Name of Kubernetes serviceaccount"
  default     = ""
}

variable "cluster_name" {
  description = "Name of Kubernetes cluster"
  default     = ""
}

variable "policy_arn" {
  description = "Policy arn to apply to the irsa role"
  default     = ""
}

variable "oidc_issuer_url" {
  description = "EKS cluster OIDC url"
  default     = ""
}

variable "aws_account_id" {
  description = "AWS account id to configure irsa role"
  default     = ""
}