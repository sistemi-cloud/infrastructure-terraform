output "fluxcd_kms_policy" {
  value = aws_iam_policy.fluxcd_kms_policy.arn
}

output "cert_manager_policy" {
  value = aws_iam_policy.cert_manager_policy.arn
}

output "external_dns_policy" {
  value = aws_iam_policy.external_dns_policy.arn
}