variable "tags" {
  type = map(string)
  default = {}
}

variable "cluster_name" {
  type = string
  default = ""
}