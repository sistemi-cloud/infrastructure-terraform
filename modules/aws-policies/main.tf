data "aws_iam_policy_document" "fluxcd_kms_policy_document" {
  statement {
    effect  = "Allow"
    actions = [ "kms:Decrypt", "kms:DescribeKey" ]
    #resources = [ "arn:aws:kms:eu-west-1:591270305876:key/d03a6eaf-9e9e-497d-975f-5f394bcce3c8" ]
    resources = [ "arn:aws:kms:eu-west-1:679360049118:key/01f22a48-cd6f-4563-8c83-2aaeb39f3e14" ]
  }
}

resource "aws_iam_policy" "fluxcd_kms_policy" {
  name = "${var.cluster_name}-fluxcd-kms-policy"
  policy = data.aws_iam_policy_document.fluxcd_kms_policy_document.json
  tags = var.tags
}

data "aws_iam_policy_document" "cert_manager_policy_document" {
  statement {
    effect  = "Allow"
    actions = ["route53:GetChange"]
    resources = [ "arn:aws:route53:::change/*" ]
  }

  statement {
    effect  = "Allow"
    actions = ["route53:ChangeResourceRecordSets", "route53:ListResourceRecordSets"]
    resources = [ "arn:aws:route53:::hostedzone/*" ]
  }

  statement {
    effect  = "Allow"
    actions = ["route53:ListHostedZonesByName"]
    resources = [ "*" ]
  }
}

resource "aws_iam_policy" "cert_manager_policy" {
  name = "${var.cluster_name}-cert-manager-policy"
  policy = data.aws_iam_policy_document.cert_manager_policy_document.json
  tags = var.tags
}

data "aws_iam_policy_document" "external_dns_policy_document" {
  statement {
    effect  = "Allow"
    actions = [ "route53:ChangeResourceRecordSets"]
    resources = [ "arn:aws:route53:::hostedzone/*" ]
  }

  statement {
    effect  = "Allow"
    actions = [ "route53:ListHostedZones", "route53:ListResourceRecordSets"]
    resources = [ "*" ]
  }
}

resource "aws_iam_policy" "external_dns_policy" {
  name = "${var.cluster_name}-external-dns-policy"
  policy = data.aws_iam_policy_document.external_dns_policy_document.json
  tags = var.tags
}
